(function (window, document, undefined) {
	window.onload = () => {
		initTextArea('sql-input', 'rawSql');
		initTextArea('sql-output', 'formattedSql');
	};

	function initTextArea(textAreaId, localStorageId) {
		document
			.getElementById(textAreaId)
			.addEventListener('keydown', function (e) {
				if (e.key == 'Tab') {
					e.preventDefault();
					var start = this.selectionStart;
					var end = this.selectionEnd;

					// set textarea value to: text before caret + tab + text after caret
					this.value =
						this.value.substring(0, start) + '\t' + this.value.substring(end);

					// put caret at right position again
					this.selectionStart = this.selectionEnd = start + 1;
				}
			});

		document.getElementById(textAreaId).addEventListener('input', function (e) {
			localStorage.setItem(localStorageId, this.value);
		});

		document.getElementById(textAreaId).value =
			localStorage.getItem(localStorageId);
	}
})(window, document, undefined);

function beautify() {
	let rawSql = document.getElementById('sql-input').value;

	var tokens = tokenize(rawSql);
	// console.log(tokens);

	var sql = parse(tokens);
	// console.log(sql);

	var formattedSql = sql.join(' ').replace(/[\t] /gm, '\t');

	document.getElementById('sql-output').value = formattedSql;

	localStorage.setItem('formattedSql', formattedSql);
}

function tokenize(rawSql) {
	return rawSql
		.replace(/^\s+|\s+$/g, '')
		.replace(/[(),]/g, ' $& ')
		.split(/\s+/gm);
}

function parse(tokens) {
	let result = [];

	while (tokens.length !== 0) {
		var token = tokens.shift();

		switch (token.toUpperCase()) {
			case 'SELECT':
				result.push('SELECT');
				break;

			case 'FROM':
				result.push('\nFROM');
				break;

			case 'JOIN':
				result.push('\nJOIN');
				break;

			case 'INNER':
				// Read join
				tokens.shift(); // throw if not join?

				result.push('\nINNER');
				result.push('JOIN');
				break;

			case 'LEFT':
				// Read outer join
				tokens.shift(); // throw if not outer?
				tokens.shift(); // throw if not join?

				result.push('\nLEFT');
				result.push('OUTER');
				result.push('JOIN');
				break;

			case 'OUTER':
				// Read apply
				tokens.shift(); // throw if not apply?

				result.push('\nOUTER');
				result.push('APPLY');
				break;

			case 'CROSS':
				// Read apply
				tokens.shift(); // throw if not apply?

				result.push('\nCROSS');
				result.push('APPLY');
				break;

			case 'WHERE':
				result.push('\nWHERE');
				break;

			case 'GROUP':
				// Read by
				tokens.shift(); // throw if not by?

				result.push('\nGROUP');
				result.push('BY');
				break;

			case 'HAVING':
				result.push('\nHAVING');
				break;

			case 'ORDER':
				// Read by
				tokens.shift(); // throw if not by?

				result.push('\nORDER');
				result.push('BY');
				break;

			case ',':
				result.push('\n,');
				break;

			case 'AND':
				result.push('\n\tAND');
				break;

			case 'OR':
				result.push('OR');
				break;

			case 'AS':
				result.push('AS');
				break;

			case 'ON':
				result.push('ON');
				break;

			case 'EXISTS':
				result.push('EXISTS');
				break;

			case 'IN':
				result.push('IN');
				break;

			case 'COALESCE':
				result.push('COALESCE');
				break;

			case 'CASE':
				result.push('CASE');
				break;

			case 'THEN':
				result.push('THEN');
				break;

			case 'WHEN':
				result.push('\n\t\tWHEN');
				break;

			case 'ELSE':
				result.push('\n\t\tELSE');
				break;

			case 'END':
				result.push('\n\tEND');
				break;

			case '(':
				let currentIndentationLevel = 0;

				for (let i = result.length - 1; i >= 0; i--) {
					let newLineCandidate = result[i];

					console.log(`Checking candidate: ${newLineCandidate}`);

					if (newLineCandidate.match(/\n/)) {
						console.log(`Match! ${newLineCandidate}`);
						console.log(
							`Tabs: ${(newLineCandidate.match(/\t/gm) || []).length}`
						);

						currentIndentationLevel = (newLineCandidate.match(/\t/gm) || [])
							.length;

						break;
					}
				}

				console.log(`Using indentation level: ${currentIndentationLevel}`);

				result.push(`\n${'\t'.repeat(currentIndentationLevel)}(`);
				const innerTokens = [];
				let level = 1;

				while (level !== 0) {
					let innerToken = tokens.shift();

					if (innerToken === '(') {
						innerTokens.push('(');
						level++;
					} else if (innerToken === ')') {
						level--;

						if (level !== 0) {
							innerTokens.push(innerToken);
						}
					} else {
						innerTokens.push(innerToken);
					}
				}

				let parsedTokens = parse(innerTokens);

				if (!parsedTokens[0].startsWith('\n')) {
					parsedTokens.unshift('\n');
				}

				for (let i = 0; i < parsedTokens.length; i++) {
					result.push(
						parsedTokens[i].replace(
							/\n/g,
							`\n${'\t'.repeat(1 + currentIndentationLevel)}`
						)
					);
				}

				result.push(`\n${'\t'.repeat(currentIndentationLevel)})`);

				break;

			default:
				result.push(token);
				break;
		}
	}

	return result;
}
